import React from 'react';
import Emoji from './Emoji'
function App() {
  return (
    <div className="App">
      <Emoji />
    </div>
  );
}

export default App;
